//
//  ObservableEvent.swift
//  KVOAlternatives
//
//  Created by Jose Mena on 8/27/19.
//  Copyright © 2019 Jose Mena. All rights reserved.
//

import Foundation

public enum ObservableEvent<T> {
    case next(T)
    case failed(Error)
    case completed
}

extension ObservableEvent {
    
    /// Return `true` in case of `.failure` or `.completed` event.
    public var isTerminal: Bool {
        switch self {
        case .next:
            return false
        default:
            return true
        }
    }
    
    /// Returns the next element, or nil if the event is not `.next`
    public var element: T? {
        switch self {
        case .next(let element):
            return element
        default:
            return nil
        }
    }
    
    /// Return the failed error, or nil if the event is not `.failed`
    public var error: Error? {
        switch self {
        case .failed(let error):
            return error
        default:
            return nil
        }
    }
}
