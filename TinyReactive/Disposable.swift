//
//  Disposable.swift
//  KVOAlternatives
//
//  Created by Jose Mena on 8/28/19.
//  Copyright © 2019 Jose Mena. All rights reserved.
//

import Foundation

public protocol Disposable {
    func dispose()
    func dispose(by bag: DisposeBag)
    var isDisposed: Bool { get }
}

public extension Disposable {
    func dispose(by bag: DisposeBag) {
        bag.disposables.append(self)
    }
}

public class EmptyDisposable: Disposable {
    public func dispose() {
    }
    public var isDisposed: Bool {
        return true
    }
}

public class BlockDisposable: Disposable {
    
    private let lock: NSRecursiveLock
    private var handler: (() -> Void)?
    
    public var isDisposed: Bool {
        lock.lock(); defer { lock.unlock() }
        return handler == nil
    }
    
    public init (_ handler: @escaping (() -> Void)) {
        lock = NSRecursiveLock()
        lock.name = "block_disposable"
        self.handler = handler
    }
    
    public func dispose() {
        lock.lock()
        defer { lock.unlock() }
        handler?()
        handler = nil
    }
}

/// A disposable that disposes other disposable upon its own disposing.
public final class SerialDisposable: Disposable {
    
    private let lock: NSRecursiveLock
    
    private var _isDisposed = false
    public var isDisposed: Bool {
        lock.lock(); defer { lock.unlock() }
        return _isDisposed
    }
    
    /// Will dispose other disposable immediately if self is already disposed.
    public var otherDisposable: Disposable? {
        didSet {
            lock.lock(); defer { lock.unlock() }
            if _isDisposed {
                otherDisposable?.dispose()
            }
        }
    }
    
    public init(otherDisposable: Disposable?) {
        self.otherDisposable = otherDisposable
        lock = NSRecursiveLock()
        lock.name = "serial_disposable"
    }
    
    public func dispose() {
        lock.lock(); defer { lock.unlock() }
        if !_isDisposed {
            _isDisposed = true
            otherDisposable?.dispose()
        }
    }
}

/// A disposable that disposes a collection of disposables upon its own disposing.
public final class CompositeDisposable: Disposable {
    
    private let lock: NSRecursiveLock
    
    private var _isDisposed = false
    public var isDisposed: Bool {
        lock.lock(); defer { lock.unlock() }
        return _isDisposed
    }
    
    private var disposables: [Disposable] = []
    
    public convenience init() {
        self.init([])
    }
    
    public init(_ disposables: [Disposable]) {
        lock = NSRecursiveLock()
        lock.name = "composite_disposable"
        self.disposables = disposables
    }
    
    public func add(disposable: Disposable) {
        lock.lock(); defer { lock.unlock() }
        if _isDisposed {
            disposable.dispose()
        } else {
            disposables.append(disposable)
            self.disposables = disposables.filter { $0.isDisposed == false }
        }
    }
    
    public static func += (left: CompositeDisposable, right: Disposable) {
        left.add(disposable: right)
    }
    
    public func dispose() {
        lock.lock(); defer { lock.unlock() }
        _isDisposed = true
        disposables.forEach { $0.dispose() }
        disposables.removeAll()
    }
}
