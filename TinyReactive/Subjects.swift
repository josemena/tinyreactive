//
//  Subjects.swift
//  KVOAlternatives
//
//  Created by Jose Mena on 8/28/19.
//  Copyright © 2019 Jose Mena. All rights reserved.
//

import Foundation

public protocol SubjectProtocol: PublisherProtocol, ObserverProtocol {
}

extension SubjectProtocol {
    public func send(_ element: T) {
        on(.next(element))
    }
}

open class Subject<T> : SubjectProtocol {

    private let lock: NSRecursiveLock
    private let deletedObserversLock: NSRecursiveLock
    private var observers: [(Int64, Observer<T>)] = []
    private var deletedObservers = Set<Int64>()
    private var nextToken: Int64 = 0
    private var isTerminatedInternal = false

    public var isTerminated: Bool {
        lock.lock()
        defer { lock.unlock() }
        return isTerminatedInternal
    }

    public init() {
        lock = NSRecursiveLock()
        lock.name = "subject"
        deletedObserversLock = NSRecursiveLock()
        deletedObserversLock.name = "subject.deleted_observers"
    }
    
    public func observe(with observer: @escaping Observer<T>) -> Disposable {
        lock.lock()
        defer { lock.unlock() }
        return add(observer: observer)
    }
    
    public func on(_ event: ObservableEvent<T>) {
        lock.lock()
        defer { lock.unlock() }
        guard !isTerminatedInternal else {
            return
        }
        isTerminatedInternal = event.isTerminal
        deletedObserversLock.lock()
        let deletedObserversCopy = deletedObservers
        deletedObserversLock.unlock()

        observers = observers.filter { (token, _) in
            !deletedObserversCopy.contains(token)
        }

        for (_, observer) in observers {
            observer(event)
        }

        deletedObserversLock.lock()
        deletedObservers = deletedObservers.subtracting(deletedObserversCopy)
        deletedObserversLock.unlock()
    }
    
    private func add(observer: @escaping Observer<T>) -> Disposable {
        let token = nextToken
        nextToken += 1
        observers.append((token, observer))
        return BlockDisposable { [weak self] in
            self?.deletedObserversLock.lock()
            defer { self?.deletedObserversLock.unlock() }
            self?.deletedObservers.insert(token)
        }
    }
}
