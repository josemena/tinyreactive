//
//  PublisherProtocol.swift
//  KVOAlternatives
//
//  Created by Jose Mena on 8/28/19.
//  Copyright © 2019 Jose Mena. All rights reserved.
//

import Foundation

/// Represents a sequence of events.
public protocol PublisherProtocol {
    associatedtype T
    func observe(with observer: @escaping Observer<T>) -> Disposable
}

/// A signal represents a sequence of elemets
public struct Publisher<T>: PublisherProtocol {
    
    /// A closure that sends events to an observer
    public typealias Producer = (AtomicObserver<T>) -> Disposable
    
    private let producer: Producer
    
    /**
     Create a new signal given the producer closure.
     
     - Parameter producer: a closure that sends events to the observer
     
     - Returns: A new Signal object.
     
     */
    public init(producer: @escaping Producer) {
        self.producer = producer
    }
    
    /** Register the observer that will receive events from the signal.
     - Parameters:
        - observer: Observer object that will receive events
     - Returns: A Disposable
     
    */
    public func observe(with observer: @escaping Observer<T>) -> Disposable {
        let serialDisposable = SerialDisposable(otherDisposable: nil)
        let atomicObserver = AtomicObserver(disposable: serialDisposable, observer: observer)
        serialDisposable.otherDisposable = producer(atomicObserver)
        return atomicObserver.disposable
    }
}

extension PublisherProtocol {

    /**
     Register an observer that will receive elements from `.next` events of the signal.
     - Parameters:
        - observer: Observer object that will receive events
     - Returns: A Disposable
     
     Observing next events on a Signal:
     
     ```
     let someSignal = getASignal()
     someSignal.observeNext { value in
         print(value)
     }.dispose(by: bag)
     ```
    */
    public func observeNext(with observer: @escaping (T) -> Void) -> Disposable {
        return observe { event in
            if case .next(let element) = event {
                observer(element)
            }
        }
    }

    /// Register an observer that will receive elements from `.failed` events of the signal.
    public func observeFailed(with observer: @escaping (Error) -> Void) -> Disposable {
        return observe { event in
            if case .failed(let error) = event {
                observer(error)
            }
        }
    }
    
    /// Register an observer that will be executed on `.completed` event.
    public func observeCompleted(with observer: @escaping () -> Void) -> Disposable {
        return observe { event in
            if case .completed = event {
                observer()
            }
        }
    }
}

extension PublisherProtocol {
    
    /// Set the dispatch queue on which to execute the signal (i.e. on which to run
    /// the signal's producer).
    public func executeOn(_ queue: DispatchQueue) -> Publisher<T> {
        return Publisher { observer in
            let serialDisposable = SerialDisposable(otherDisposable: nil)
            queue.async {
                if !serialDisposable.isDisposed {
                    serialDisposable.otherDisposable = self.observe(with: observer.on)
                }
            }
            return serialDisposable
        }
    }
    
    /// Set the dispatch queue used to dispatch events (i.e. to run the observers).
    public func observeOn(_ queue: DispatchQueue) -> Publisher<T> {
        return Publisher { observer in
            return self.observe { event in
                queue.async {
                    observer.on(event)
                }
            }
        }
    }
}
