//
//  Event.swift
//  KVOAlternatives
//
//  Created by Jose Mena on 8/27/19.
//  Copyright © 2019 Jose Mena. All rights reserved.
//

import Foundation

private protocol Invocable: AnyObject {
    func invoke(data: Any)
}

private class EventHandlerWrapper<T: AnyObject, U>: Invocable, Disposable {
    
    weak var target: T?
    let handler: (T) -> (U) -> ()
    let event: Event<U>

    var isDisposed: Bool {
        return event.invocables.map { $0 !== self }.reduce(false) { $0 || $1 }
    }
    
    init(target: T?, handler: @escaping (T) -> (U) -> (), event: Event<U>) {
        self.target = target
        self.handler = handler
        self.event = event
    }
    
    func invoke(data: Any) {
        if let target = target, let data = data as? U {
            handler(target)(data)
        }
    }
    
    func dispose() {
        event.invocables = event.invocables.filter { $0 !== self }
    }

    func dispose(by bag: DisposeBag) {
        bag.disposables.append(self)
    }
}

public class Event<T> {
    public typealias EventHandler = (T) -> ()
    fileprivate var invocables = [Invocable]()
    
    func addHandler<U: AnyObject>(target: U, handler: @escaping (U) -> EventHandler) -> Disposable {
        let wrapper = EventHandlerWrapper(target: target, handler: handler, event: self)
        invocables.append(wrapper)
        return wrapper
    }
    
    func raise(data: T) {
        invocables.forEach { invocable in
            invocable.invoke(data: data)
        }
    }
}
