//
//  Observer.swift
//  KVOAlternatives
//
//  Created by Jose Mena on 8/27/19.
//  Copyright © 2019 Jose Mena. All rights reserved.
//

import Foundation

public typealias Observer<T> = (ObservableEvent<T>) -> Void

public protocol ObserverProtocol {
    associatedtype T

    func on(_ event: ObservableEvent<T>)
}

public extension ObserverProtocol {
    func receive(_ element: T) {
        on(.next(element))
    }

    func complete() {
        on(.completed)
    }
}

public struct AnyObserver<T>: ObserverProtocol {
    
    private let observer: Observer<T>
    
    public init(observer: @escaping Observer<T>) {
        self.observer = observer
    }

    public func on(_ event: ObservableEvent<T>) {
        observer(event)
    }
}

public final class AtomicObserver<T>: ObserverProtocol {
    
    private var observer: Observer<T>?
    private let lock: NSRecursiveLock
    private let parentDisposable: Disposable

    private var internalDisposable: Disposable?

    public var disposable: Disposable {
        lock.lock(); defer { lock.unlock() }
        return internalDisposable ?? EmptyDisposable()
    }

    public init(disposable: Disposable, observer: @escaping Observer<T>) {
        self.observer = observer
        self.parentDisposable = disposable
        lock = NSRecursiveLock()
        lock.name = "atomic_observer"
    
        lock.lock()
        defer { lock.unlock() }
        self.internalDisposable = BlockDisposable { [weak self] in
            self?.observer = nil
            disposable.dispose()
        }
    }

    public func on(_ event: ObservableEvent<T>) {
        lock.lock()
        defer { lock.unlock() }
        guard let internalDisposable = internalDisposable, !internalDisposable.isDisposed else {
            return 
        }
        observer?(event)
        switch event {
        case .failed(_), .completed:
            internalDisposable.dispose()
        default:
            break
        }
    }
}
