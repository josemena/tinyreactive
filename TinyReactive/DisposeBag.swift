//
//  DisposeBag.swift
//  KVOAlternatives
//
//  Created by Jose Mena on 8/27/19.
//  Copyright © 2019 Jose Mena. All rights reserved.
//

import Foundation

public class DisposeBag {
    var disposables = [Disposable]()

    deinit {
        disposables.forEach { $0.dispose() }
    }
}
