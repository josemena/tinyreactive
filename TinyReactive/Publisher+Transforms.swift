//
//  Signal+Transforms.swift
//  KVOAlternatives
//
//  Created by Jose Mena on 8/30/19.
//  Copyright © 2019 Jose Mena. All rights reserved.
//

import Foundation

// Transforms
extension PublisherProtocol {
    public func map<U>(_ transform: @escaping (T) -> U) -> Publisher<U> {
        return Publisher { observer in
            return self.observe { event in
                switch event {
                case .next(let element):
                    observer.on(.next(transform(element)))
                case .failed(let error):
                    observer.on(.failed(error))
                case .completed:
                    observer.on(.completed)
                }
            }
        }
    }

    public func flatMap<U: PublisherProtocol>(_ transform: @escaping (T) -> U) -> Publisher<U.T> {
        return self.map(transform).merge()
    }

}

extension PublisherProtocol where T: PublisherProtocol {
    func merge() -> Publisher<T.T> {
        return Publisher { observer in
            let lock = NSRecursiveLock()
            lock.name = "publisher_merge"
            let compositeDisposable = CompositeDisposable()
            var numberOfOperations = 1
            
            func decrementNumberOfOperations() {
                numberOfOperations -= 1
                if numberOfOperations == 0 {
                    observer.on(.completed)
                }
            }
            compositeDisposable += self.observe  { (outerEvent: ObservableEvent<T>) in
                switch outerEvent  {
                case .next(let innerPublisher):
                    lock.lock()
                    defer {
                        lock.unlock()
                    }
                    numberOfOperations += 1
                    compositeDisposable  += innerPublisher.observe { (innerEvent: ObservableEvent<T.T>) in
                        switch innerEvent  {
                        case .next(let element):
                            observer.on(.next(element))
                        case .failed(let error):
                            observer.on(.failed(error))
                        case .completed:
                            lock.lock()
                            defer {
                                lock.unlock()
                            }
                            decrementNumberOfOperations()
                        }
                    }
                case .failed(let error):
                    observer.on(.failed(error))
                case .completed:
                    lock.lock()
                    defer {
                        lock.unlock()
                    }
                    decrementNumberOfOperations()
                }
            }
            return compositeDisposable
        }
    }
}
